from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy

import os
import datetime, pytz

app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://root:secret@localhost:5432/postgres'
db = SQLAlchemy(app)

class Mail(db.Model):
  event_id = db.Column(db.Integer(), primary_key=True)
  email_subject = db.Column(db.String(80), unique=False, nullable=False)
  email_content = db.Column(db.String(120), unique=False, nullable=False)
  timestamp = db.Column(db.DateTime(timezone=True), unique=False, nullable=False)
  status = db.Column(db.String(20), unique=False, nullable=False)

  def __init__(self,event_id, email_subject, email_content,timestamp, status):
    self.event_id = event_id
    self.email_subject = email_subject
    self.email_content = email_content
    self.timestamp = timestamp
    self.status = status

db.create_all()

@app.route('/save_emails', methods=['GET'])
def get_mails():
  mails = []
  data = db.session.query(Mail)
  for mail in data.order_by(Mail.event_id):
    del mail.__dict__['_sa_instance_state']
    mails.append(mail.__dict__)
  return jsonify(mails)

@app.route('/save_emails', methods=['POST'])
def create_mails():
  body = request.get_json()
  db.session.add(Mail(body['event_id'],
                      body['email_subject'],
                      body['email_content'],
                      body['timestamp'],status='scheduled'))
  db.session.commit()
  return "item created"

@app.route('/save_emails/<id>', methods=['GET'])
def get_mail_by_id(id):
  timezone = pytz.timezone('Asia/Singapore')
  try:
    mail = Mail.query.get(id)
    del mail.__dict__['_sa_instance_state']
    local_dt = mail.timestamp.replace(tzinfo=pytz.utc).astimezone(timezone)
    print(local_dt)
    print(timezone.localize(datetime.datetime.now()))
    print(mail.timestamp)
    print(local_dt<timezone.localize(datetime.datetime.now()))
    return jsonify(event_id=mail.event_id, timestamp=mail.timestamp)
  except :
    return jsonify("no data found")

@app.route('/save_emails/<id>', methods=['PUT'])
def update_mails(id):
  body = request.get_json()
  db.session.query(Mail).filter_by(event_id=id).update(
    dict(status=body['status']))
  db.session.commit()
  mail = Mail.query.get(id)
  del mail.__dict__['_sa_instance_state']
  return jsonify(mail.event_id,"was sent")

@app.route('/send_emails', methods=['GET'])
def sending_mail():
  timezone = pytz.timezone('Asia/Singapore')
  for mail in db.session.query(Mail).all():
    local_dt = mail.timestamp.replace(tzinfo=pytz.utc).astimezone(timezone)
    # del mail.__dict__['_sa_instance_state']
    if local_dt<timezone.localize(datetime.datetime.now()):
      db.session.query(Mail).filter_by(event_id=mail.event_id).update(
      dict(status="sent"))
      db.session.commit()
      print(str(mail.event_id)+" Yes")
    else:
      print(str(mail.event_id)+" No")
  return "checked"

@app.route('/get_time', methods=['GET'])
def get_time():
  timezone = pytz.timezone('Asia/Singapore')
  return jsonify(timezone.localize(datetime.datetime.now()))


if __name__ == '__main__':
  app.run(port=80)
