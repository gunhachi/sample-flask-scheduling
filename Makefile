DB_URL=postgresql://root:secret@localhost:5432/postgres?sslmode=disable

postgres:
	docker run --name postgres-test -p 5432:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=secret -d postgres:latest

createdb:
	docker exec -it postgres-test createdb --username=root --owner=root postgres

.PHONY: postgres createdb
