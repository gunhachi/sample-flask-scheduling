FROM python:3.6-slim-buster

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY app.py .

# EXPOSE 80

CMD ["python","app.py"]