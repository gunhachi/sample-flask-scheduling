from app import Mail
import pytz, datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler
import os

app = Flask(__name__)
scheduler = APScheduler()
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://root:secret@localhost:5432/postgres'
db = SQLAlchemy(app)

def sendMailTask():
  timezone = pytz.timezone('Asia/Singapore')

  for mail in db.session.query(Mail).all():
    local_dt = mail.timestamp.replace(tzinfo=pytz.utc).astimezone(timezone)
    if local_dt<=timezone.localize(datetime.datetime.now()):
      db.session.query(Mail).filter_by(event_id=mail.event_id).update(
      dict(status="sent"))
      db.session.commit()
      print("event id :"+str(mail.event_id)+" sent")
    else:
      print("event id :"+str(mail.event_id)+" scheduled")

if __name__ == '__main__':
  scheduler.add_job(id = 'Scheduled Task', func=sendMailTask, trigger="interval", seconds=3)
  scheduler.start()
  app.run(port=81)
