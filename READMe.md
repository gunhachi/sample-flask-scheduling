# Sample of Flask App Scheduling
A repository contains of scheduling endpoint baked in python microframework Flask. The context is to try automate event based on stored timestamp within database. This project use Flask-APScheduler for automate the task.

## Project structure
- app.py -> main api
- task.py -> for scheduling email 

## Run Program
1. Use python environment,(conda use in this case)
2. Use postgre by docker image with command `make postgres` 
3. Run  `pip install -r requirements`
4. Run program on separate terminal
    ```
    python app.py
    ----------------
    python task.py
    ```
## Endpoint
The endpoint could be tested with REST Client vscode extension on file api-test.http and simply click by the `send request`
- POST ip:80/save_emails to create new email data with example body
    ```
    {
    "event_id":1,
    "email_subject":"Verification Letter",
    "email_content":"We already try that one",
    "timestamp":"2022-06-21 12:00:00"
    }
    ```

- GET ip:80/save_emails to get all email data 

## Preferable to-do
- [ ] fixing docker-compose
- [ ] unit test
- [ ] better logging